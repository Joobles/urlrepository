package controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.jooble.beans.UrlRepository;

import java.net.URISyntaxException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/applicationContext.xml"})
public class UrlRepositoryTest {
    @Autowired
    UrlRepository urlRepository;

    @Before
    public void setup() throws URISyntaxException {
        urlRepository.addLink("google.com");
        urlRepository.addLink("foo.livejournal.com/post234.html?a=b");
        urlRepository.addLink("ororo.tv/shows/orange-is-new-black");
        urlRepository.addLink("youtube.com/feed/trending");
    }

    @Test
    public void getTopLinks() {
        int n = 4;
        Assert.assertEquals(4, urlRepository.getTopLinks(n).size());
    }
}
