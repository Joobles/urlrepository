package ru.jooble.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.jooble.beans.UrlRepository;

import java.net.URISyntaxException;
import java.util.List;

@Controller

@RequestMapping("/")
public class UrlController {

    @Autowired
    UrlRepository urlRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String showPageTopLinks() {
        return "topLinks";
    }

    @RequestMapping(value = "/add/{link}/**", method = RequestMethod.GET)
    public ResponseEntity addLink(@PathVariable("link") String link) {
        try {
            urlRepository.addLink(link);
            return new ResponseEntity(HttpStatus.OK);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/top/{n}", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getTopLinks(@PathVariable("n") Integer n) {
        return urlRepository.getTopLinks(n);
    }
}