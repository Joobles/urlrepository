package ru.jooble.beans;

import java.net.URISyntaxException;
import java.util.List;

public interface UrlRepository {
    void addLink(String link) throws URISyntaxException;

    List<String> getTopLinks(Integer n);
}

