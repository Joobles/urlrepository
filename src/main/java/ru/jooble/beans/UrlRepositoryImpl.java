package ru.jooble.beans;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


@Component
@EnableAsync
public class UrlRepositoryImpl implements UrlRepository {

    /*таймаут сортировки в млс*/
    private static final int SORTING_TIMEOUT = 30000;
    /*файл в который идет сохранение ссылок*/
    private static final String FILE_NAME = "repository.txt";

    private ConcurrentHashMap<String, AtomicInteger> repository;
    private List<Map.Entry<String, AtomicInteger>> list;

    @PostConstruct
    private void initializationRepository() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            repository = (ConcurrentHashMap<String, AtomicInteger>) ois.readObject();
            list = new ArrayList<>(repository.entrySet());
        } catch (Exception e) {
            repository = new ConcurrentHashMap<>();
            list = new ArrayList<>(repository.entrySet());
        }
    }

    @PreDestroy
    private void saveRepository() {
        try (FileOutputStream fileOutputStream = new FileOutputStream(FILE_NAME);
             ObjectOutput objectOutput = new ObjectOutputStream(fileOutputStream)) {
            objectOutput.writeObject(repository);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void addLink(String link) throws URISyntaxException {
        URI uri;

        if (link.startsWith("http://") || link.startsWith("https://")) {
            uri = new URI(link);
        } else {
            uri = new URI("http://" + link);
        }

        String[] arrayUrl = uri.getHost().split("\\.");
        String url;

        if (arrayUrl.length == 2) {
            url = uri.getHost();
        } else {
            url = arrayUrl[arrayUrl.length - 2] + "." + arrayUrl[arrayUrl.length - 1];
        }

        if (repository.containsKey(url)) {
            repository.get(url).incrementAndGet();
        } else {
            repository.put(url, new AtomicInteger(1));
        }
    }

    @Override
    public List<String> getTopLinks(Integer n) {
        if (n > list.size()) {
            n = list.size();
        }

        List<String> topList = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            topList.add(list.get(i).getKey());
        }

        return topList;
    }

    @Scheduled(fixedDelay = SORTING_TIMEOUT)
    private void sortedMap() {
        list = new ArrayList<>(repository.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, AtomicInteger>>() {
            @Override
            public int compare(Map.Entry<String, AtomicInteger> a, Map.Entry<String, AtomicInteger> b) {
                return b.getValue().get() - a.getValue().get();
            }
        });
    }


}
