<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
    <title>Top Links</title>
    <jsp:include page="setupPageJsp.jsp" flush="true"/>
</head>
<body>
<div class="container">
    <h1><p class="text-center">Top Links</p></h1>
    <div class="col-xs-1">
        <select id="numberDisplayedLinks" class="form-control input-sm">
            <option value="1" selected="selected">1</option>
            <option value="5">5</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
        </select>
    </div>
    <br>
    <p align="right">
        <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal">save link</button>
    </p>
    <div align="center">
        <table id="topLinks" class="table">
            <tr>
                <thead>
                <th>№</th>
                <th>Link</th>
                </thead>
            </tr>
        </table>
    </div>
</div>

<%--Modal Save Link--%>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Save Link</h4>
            </div>
            <div class="modal-body">
                <div align="center">
                    <form method="get" action="javascript:void(null);" id="saveLink" onsubmit="saveLink()">
                        <input id="value" type="text" placeholder="Enter link">
                        <br>
                        <br>
                        <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                        <button type="submit" class="btn btn-default">save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    setTimeout(function init() {
        getTopLink();
        setTimeout(init, 2000);
    }, 2000);
</script>
</body>
</html>