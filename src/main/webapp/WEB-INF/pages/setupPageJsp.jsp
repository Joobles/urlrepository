<script type="text/javascript" src="resources/plugins/jQuery/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="resources/plugins/bootstrap/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="resources/plugins/bootstrap/css/bootstrap.min.css">

<base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>

<script type="text/javascript" src="resources/js/main.js"></script>